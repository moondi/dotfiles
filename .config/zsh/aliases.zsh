############# General Aliases #############
alias la='ls -Alh --color=auto'
alias l='ls -Alh --color=auto'
alias ll='ls -Al --color=auto'
alias rcp='rsync --archive --progress --human-readable'
alias ppp='echo -e ${PATH//:/\\n}' # pretty print path =P
alias watch='watch ' # small hack to get watch to work with aliases (https://askubuntu.com/a/1386212)

alias nr='npm run'
alias ff='open -a firefox'
alias cr='open -a "google chrome"'

# navigation
alias cd..='cd ..'
alias dev="$DEV"
alias gosrc="$DEV/go/src"

if [[ "$os_type" == 'windows' ]]; then
  alias cdev='/mnt/c/dev' # for windows-specific dotfiles
  alias ddev='/mnt/d/dev' # for any development work that MUST live in the windows filesystem
  alias cdotfiles='/mnt/c/dev/dotfiles'
fi
############# General Aliases #############



############# Git Aliases #############
### core

# hacky, but suuuuper helpful
# - it's really annoying that `git commit -a` doesn't warn you when you have partial changes already staged
# - the pre-commit hook cannot be used for this, as all the changes are already staged at that point
#   I couldn't find any way to see what the staged changes were before the '-a' took effect
git() {
  if [[ $1 == "commit" && $2 == "-am" && # commiting with --all
        $(git diff --staged --numstat | wc -l) -ne 0 && # has staged changes
        $(git diff --numstat | wc -l) -ne 0 # AND has unstaged changes
     ]]; then
    echo "You're committing with --all while you already have staged changes!"
    echo "use \`gaa\` to stage every and re-run the commit if that was intentional"
  else
    command git "$@" # pass through all other git commands
  fi
}

# navigation/context switching
alias gl='git lg'
alias gco='git checkout'
alias gs='git status'
alias gd='git diff'
alias gdm='git diff --diff-algorithm=minimal' # myers (default), with heuristics disabled
alias gdp='git diff --diff-algorithm=patience' # better when re-ordering methods
alias gdh='git diff --diff-algorithm=histogram' # extension of 'patience' to 'support low-occurrence common elements'
alias gds='git diff --staged'
# committing
alias ggf='git commit -m "f" --no-verify' # means 'fixup later' - often used with `git add -p` and `gaa`
alias gaa='git add --all'
alias ggcommit='git commit --amend --no-edit'
alias ggwip='git commit -am "wip" --no-verify' # use w/`git reset HEAD^` instead of stashes (which are pointless imo)
# rebasing
alias gro='git rebase --onto' # gro desired-target point-to-rebase-from branch-to-rebase (ie. `gro master 123456 feat`)
alias grc='git rebase --continue'
alias grs='git rebase --skip'
# branch management
alias gb='git branch -vv'
alias grb='git recentb'
alias gbr='echo "git branch --remote" && git branch --remote'
alias gbm='echo "git branch --merged" && git branch --merged' # branches that can be cleaned up with `gb -d`
alias gbg='gb --color=always --sort=committerdate | grep -E "gone|$"' # to get color for 'gone'

### auxilliary
# alias gbd='git-branch-descriptions'
# alias gbs='git-branch-status'
alias gcoz='git checkout --conflict=zdiff3'
alias gcod='git checkout --conflict=diff3'
alias gcom='git checkout --conflict=merge'
alias ggd='git commit -m "d" --no-verify' # means 'delete later', not used often

# handy for ephemoral todo's within a branch; these should NEVER be merged into master
todo() { git commit --allow-empty -m "TODO: $*" --no-verify }
note() { git commit --allow-empty -m "NOTE: $*" --no-verify }

# git branch build info
# - shortsha and date are OK, but this works better for me with how much I like to rewrite within branches
# - {YYYY-MM-DD}-{branchname}-{shortsha}-{basebranch delta}
gbbi() {
  basebranch=${1:-master}
  sep=${2:-_}

  echo "\
$(date '+%Y-%m-%d')\
${sep}\
$(git rev-parse --abbrev-ref HEAD)\
${sep}\
$(git rev-parse --short HEAD)\
${sep}\
${basebranch}\
+$(git rev-list --count ${basebranch}..HEAD)\
"
}

### graph log variants
alias gla='git lg --all'
alias glt='git lgtips --all'
alias glta='git lgtips --all'

# this alias does not work with any additional arguments intended for git log
# `sed` commentary:
# - abbreviate Thu to R
# - abbreviate Sun to U
# - abbreviate Mon/Tue/Wed/Fri/Sat to M/T/W/F/S
#   - with thurs/sun out of the way, we can just truncate the remaining characters
# - abbreviate Feb/Sep/Dec to FE/SE/DE
#   - these are all the month cases where we can simply capitalize the E (BRE does not have \U to change case)
# - Jan, Apr, May, Jun, Jul, Aug, Oct, Nov each get their own case b/c BRE is very limited (no support for alternator `|`)
# - finally, shorten the format for relative committer date
#    - had to split this into 3 passes to get rid of the |'s
gls() {
  git lgdenser $* | \
  sed "s/Thu\(\/[A-Za-z]\{3\}[0-9\/]\{5\}\)/R\1/1" | \
  sed "s/Sun\(\/[A-Za-z]\{3\}[0-9\/]\{5\}\)/U\1/1" | \
  sed "s/\([MTWFS]\)[a-z]\{2\}\(\/[A-Za-z]\{3\}[0-9\/]\{5\}\)/\1\2/1" | \
  sed "s/\(Ja\)[a-z]\([0-9\/]\{5\}\)/JA\2/1" | \
  sed "s/\([FSD]\)[a-z][a-z]\([0-9\/]\{5\}\)/\1E\2/1" | \
  sed "s/\(Mar\)\([0-9\/]\{5\}\)/MR\2/1" | \
  sed "s/\(Ap\)[a-z]\([0-9\/]\{5\}\)/AP\2/1" | \
  sed "s/\(May\)\([0-9\/]\{5\}\)/MY\2/1" | \
  sed "s/\(Jun\)\([0-9\/]\{5\}\)/JN\2/1" | \
  sed "s/\(Jul\)\([0-9\/]\{5\}\)/JL\2/1" | \
  sed "s/\(Au\)[a-z]\([0-9\/]\{5\}\)/AU\2/1" | \
  sed "s/\(Oc\)[a-z]\([0-9\/]\{5\}\)/OC\2/1" | \
  sed "s/\(Nov\)\([0-9\/]\{5\}\)/NV\2/1" | \
  sed "s/(\([0-9][0-9]*\) \(min\)[a-z][a-z]*[,]*\( *\)\([0-9]*\)[ ]*\([mo]*\)[a-z]* ago)/(\1\2\3\4\5)/1" | \
  sed "s/(\([0-9][0-9]*\) \(mo\)[a-z][a-z]*[,]*\( *\)\([0-9]*\)[ ]*\([mo]*\)[a-z]* ago)/(\1\2\3\4\5)/1" | \
  sed "s/(\([0-9][0-9]*\) \([ywdhs]\)[a-z][a-z]*[,]*\( *\)\([0-9]*\)[ ]*\([mo]*\)[a-z]* ago)/(\1\2\3\4\5)/1" | \
  less
}
alias glsa='gls --all'
alias glst='gls --simplify-by-decoration'
alias glsta='gls --simplify-by-decoration --all'

############# Git Aliases #############



############# Repo Aliases #############
#
# organizational guidelines:
# - personal repos can be directly in $DEV
#   - not sure where I want forks for personal projects yet... (flat? in `forks/`? in `.forks/`? something else?)
# - org-specific repos (and forks) should be grouped in a subdirectory
# - reference libraries should be grouped into a dotted subdirectory
#   - use then language's file extension, when appropriate and relatively unambiguous
# - use other subdirectories and custom groupings as appropriate

# Personal
alias docs="$DEV/docs"
alias dotfiles="$DEV/dotfiles"
alias ds="$DEV/dotfiles"
alias hpmor="$DEV/hpmor"
alias misc="$DEV/misc"
alias stp="'$SUBL_PKG'" # sublime packages dir (path contains a space, so needs to be quoted)
alias sts="'$SUBL_PKG/User'" # moondi/sublime-settings (path contains a space, so needs to be quoted)
alias stuff="$DEV/stuff" # only relevant on my windows machine atm, but is living primarily in ubuntu (also cloned in ddev)

alias mc="$DEV/moondi-ca"
alias mca="$DEV/moondi-ca/web"
alias mcw="$DEV/moondi-ca/web"
alias mco="$DEV/moondi-ca/ops/cluster"
alias mcd="$DEV/moondi-ca/ops/dockerfiles"
alias mcp="$DEV/moondi-ca/ops/pi"
alias mck="$DEV/moondi-ca/kemal"
alias mcl="$DEV/moondi-ca/lucky"
alias mcr="$DEV/moondi-ca/rails"
alias mcb="$DEV/moondi-ca-backups"

# Rembrandt
alias rc="$DEV/rembrandt"
############# Repo Aliases #############



############# Kubernetes Aliases #############
#
# to safeguard against applying staging manifests to the production cluster:
#
# -----------------------------------------------------------------------
#  NEVER set the production cluster as the `current-context` for kubectl
# -----------------------------------------------------------------------
#
# - add the `--cluster` flag before running commands from external sources
#   - if omitted, then the command will be run on the staging cluster, which is preferable
#
alias k="kubectl" # this uses `current-context`, so be careful
alias kcontext="kubectl config use-context" # should be used sparingly, if at all
alias ktn="kubectl top --use-protocol-buffers=true nodes"
alias ktp="kubectl top --use-protocol-buffers=true pods --all-namespaces"

# general short aliases for the current context
# - included for personal use on the k3s cluster I run off my raspberry pi
# - can also be used for work, where it should apply to the staging cluster (prod should NEVER be the active context)
alias km="kubectl --namespace=monitoring"
alias kk="kubectl --namespace=kube-system"

# staging cluster
alias ks="kubectl --context=stg-read"
alias kss="kubectl --context=stg-read --namespace=staging"
alias ksm="kubectl --context=stg-read --namespace=monitoring"
alias ksk="kubectl --context=stg-read --namespace=kube-system"
alias ksi="kubectl --context=stg-read --namespace=internal"
alias ksci="kubectl --context=stg-read --namespace=ci"

alias ksn="kubectl --context=stg-read get nodes -L alpha.eksctl.io/nodegroup-name"
alias kstn="kubectl --context=stg-read top --use-protocol-buffers=true nodes"
alias kstp="kubectl --context=stg-read top --use-protocol-buffers=true pods --all-namespaces"

alias sw_ks=" kubectl --context=stg-admin"
alias sw_kss=" kubectl --context=stg-admin --namespace=staging"
alias sw_ksm=" kubectl --context=stg-admin --namespace=monitoring"
alias sw_ksk=" kubectl --context=stg-admin --namespace=kube-system"
alias sw_ksi=" kubectl --context=stg-admin --namespace=internal"
alias sw_ksci=" kubectl --context=stg-admin --namespace=ci"

# production cluster
alias kp="kubectl --context=prod-read"
alias kpp="kubectl --context=prod-read --namespace=production"
alias kpm="kubectl --context=prod-read --namespace=monitoring"
alias kpk="kubectl --context=prod-read --namespace=kube-system"
alias kpi="kubectl --context=prod-read --namespace=internal"
alias kpci="kubectl --context=prod-read --namespace=ci"

alias kpn="kubectl --context=prod-read get nodes -L alpha.eksctl.io/nodegroup-name"
alias kptn="kubectl --context=prod-read top --use-protocol-buffers=true nodes"
alias kptp="kubectl --context=prod-read top --use-protocol-buffers=true pods --all-namespaces"

alias pw_kp=" kubectl --context=prod-admin"
alias pw_kpp=" kubectl --context=prod-admin --namespace=production"
alias pw_kpm=" kubectl --context=prod-admin --namespace=monitoring"
alias pw_kpk=" kubectl --context=prod-admin --namespace=kube-system"
alias pw_kpi=" kubectl --context=prod-admin --namespace=internal"
alias pw_kpci=" kubectl --context=prod-admin --namespace=ci"
############# Kubernetes Aliases #############



############# AWS Aliases #############
alias awss="AWS_PROFILE=stg-read aws"
alias awsp="AWS_PROFILE=prod-read aws"
alias ekss="AWS_PROFILE=stg-read eksctl"
alias eksp="AWS_PROFILE=prod-read eksctl"

# GUARDRAILS:
# - command is NOT stored in the history (b/c starts w/space)
# - if you want the command to be in the history (but SAFE for accidental re-runs)
#   - run with the `read` profile first (will fail)
#   - then re-run, adding the appropriate prefix (ie. 'sw_' for 'staging write')
alias sw_awss=" AWS_PROFILE=stg-admin aws"
alias pw_awsp=" AWS_PROFILE=prod-admin aws"
alias sw_ekss=" AWS_PROFILE=stg-admin eksctl"
alias pw_eksp=" AWS_PROFILE=prod-admin eksctl"
############# AWS Aliases #############



############# Docker Aliases #############
alias d="docker"
alias di="docker images"
alias dps="docker ps"
alias dr="docker run --rm -it"
############# Docker Aliases #############
