setopt prompt_subst
autoload colors
colors

autoload -U add-zsh-hook
autoload -Uz vcs_info

# check-for-changes can be really slow.
# you should disable it, if you work with large repositories
zstyle ':vcs_info:*:prompt:*' check-for-changes true

add-zsh-hook precmd moose_precmd

moose_precmd() {
  vcs_info
}

kubernetes_context() {
  if [ -x "$(command -v kubectl)" ]; then
    context="$(kubectl config current-context 2>/dev/null)"
    if [ -n "$context" ]; then
      color="%{$fg[red]%}"
      if [ "$context" = "staging" ] || [ "$context" = "aang" ]; then
        color="%{$fg[green]%}"
      fi
      echo -n "%{$fg_bold[blue]%}(k)-[${color}${context}%{$fg_bold[blue]%}]"
    fi
  fi
}

WSL_DISTRO="" && [ -n "$WSL_DISTRO_NAME" ] && WSL_DISTRO="|$WSL_DISTRO_NAME"

# user, host, full path, and time/date
# on two lines for easier vgrepping
# entry in a nice long thread on the Arch Linux forums: http://bbs.archlinux.org/viewtopic.php?pid=521888#p521888
PROMPT=$'%{\e[0;34m%}%B┌─[%b%{\e[0m%}%{\e[1;32m%}%n%{\e[1;30m%}@%{\e[0m%}%{\e[0;36m%}%m${WSL_DISTRO}%{\e[0;34m%}%B]%b%{\e[0m%} - %b%{\e[0;34m%}%B[%b%{\e[1;35m%}%~%{\e[0;34m%}%B]%b%{\e[0m%} - %{\e[0;34m%}%B[%b%{\e[0;33m%}'%D{"%a %b %d, %I:%M"}%b$'%{\e[0;34m%}%B]%b%{\e[0m%}
%{\e[0;34m%}%B└─%B[%{\e[1;35m%}$%{\e[0;34m%}%B] <(-$(kubernetes_context)$vcs_info_msg_0_)>%{\e[0m%}%b '
PS2=$' \e[0;34m%}%B>%{\e[0m%}%b '


# from xiong-chiamiov-plus.zsh-theme
#PROMPT=$'%{\e[0;34m%}%B┌─[%b%{\e[0m%}%{\e[1;32m%}%n%{\e[1;30m%}@%{\e[0m%}%{\e[0;36m%}%m%{\e[0;34m%}%B]%b%{\e[0m%} - %b%{\e[0;34m%}%B[%b%{\e[1;37m%}%~%{\e[0;34m%}%B]%b%{\e[0m%} - %{\e[0;34m%}%B[%b%{\e[0;33m%}'%D{"%a %b %d, %I:%M"}%b$'%{\e[0;34m%}%B]%b%{\e[0m%}
#%{\e[0;34m%}%B└─%B[%{\e[1;35m%}$%{\e[0;34m%}%B] <$(git_prompt_info)>%{\e[0m%}%b '
#PS2=$' \e[0;34m%}%B>%{\e[0m%}%b '








# # oh-my-zsh Bureau Theme

# ### NVM

# ZSH_THEME_NVM_PROMPT_PREFIX="%B⬡%b "
# ZSH_THEME_NVM_PROMPT_SUFFIX=""

# ### Git [±master ▾●]

# ZSH_THEME_GIT_PROMPT_PREFIX="[%{$fg_bold[green]%}±%{$reset_color%}%{$fg_bold[white]%}"
# ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}]"
# ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg_bold[green]%}✓%{$reset_color%}"
# ZSH_THEME_GIT_PROMPT_AHEAD="%{$fg[cyan]%}▴%{$reset_color%}"
# ZSH_THEME_GIT_PROMPT_BEHIND="%{$fg[magenta]%}▾%{$reset_color%}"
# ZSH_THEME_GIT_PROMPT_STAGED="%{$fg_bold[green]%}●%{$reset_color%}"
# ZSH_THEME_GIT_PROMPT_UNSTAGED="%{$fg_bold[yellow]%}●%{$reset_color%}"
# ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg_bold[red]%}●%{$reset_color%}"

# bureau_git_branch () {
#   ref=$(command git symbolic-ref HEAD 2> /dev/null) || \
#   ref=$(command git rev-parse --short HEAD 2> /dev/null) || return
#   echo "${ref#refs/heads/}"
# }

# bureau_git_status() {
#   _STATUS=""

#   # check status of files
#   _INDEX=$(command git status --porcelain 2> /dev/null)
#   if [[ -n "$_INDEX" ]]; then
#     if $(echo "$_INDEX" | command grep -q '^[AMRD]. '); then
#       _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_STAGED"
#     fi
#     if $(echo "$_INDEX" | command grep -q '^.[MTD] '); then
#       _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_UNSTAGED"
#     fi
#     if $(echo "$_INDEX" | command grep -q -E '^\?\? '); then
#       _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_UNTRACKED"
#     fi
#     if $(echo "$_INDEX" | command grep -q '^UU '); then
#       _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_UNMERGED"
#     fi
#   else
#     _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_CLEAN"
#   fi

#   # check status of local repository
#   _INDEX=$(command git status --porcelain -b 2> /dev/null)
#   if $(echo "$_INDEX" | command grep -q '^## .*ahead'); then
#     _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_AHEAD"
#   fi
#   if $(echo "$_INDEX" | command grep -q '^## .*behind'); then
#     _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_BEHIND"
#   fi
#   if $(echo "$_INDEX" | command grep -q '^## .*diverged'); then
#     _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_DIVERGED"
#   fi

#   if $(command git rev-parse --verify refs/stash &> /dev/null); then
#     _STATUS="$_STATUS$ZSH_THEME_GIT_PROMPT_STASHED"
#   fi

#   echo $_STATUS
# }

# bureau_git_prompt () {
#   local _branch=$(bureau_git_branch)
#   local _status=$(bureau_git_status)
#   local _result=""
#   if [[ "${_branch}x" != "x" ]]; then
#     _result="$ZSH_THEME_GIT_PROMPT_PREFIX$_branch"
#     if [[ "${_status}x" != "x" ]]; then
#       _result="$_result $_status"
#     fi
#     _result="$_result$ZSH_THEME_GIT_PROMPT_SUFFIX"
#   fi
#   echo $_result
# }


# _PATH="%{$fg_bold[white]%}%~%{$reset_color%}"

# if [[ $EUID -eq 0 ]]; then
#   _USERNAME="%{$fg_bold[red]%}%n"
#   _LIBERTY="%{$fg[red]%}#"
# else
#   _USERNAME="%{$fg_bold[white]%}%n"
#   _LIBERTY="%{$fg[green]%}$"
# fi
# _USERNAME="$_USERNAME%{$reset_color%}@%m"
# _LIBERTY="$_LIBERTY%{$reset_color%}"


# get_space () {
#   local STR=$1$2
#   local zero='%([BSUbfksu]|([FB]|){*})'
#   local LENGTH=${#${(S%%)STR//$~zero/}}
#   local SPACES=""
#   (( LENGTH = ${COLUMNS} - $LENGTH - 1))

#   for i in {0..$LENGTH}
#     do
#       SPACES="$SPACES "
#     done

#   echo $SPACES
# }

# _1LEFT="$_USERNAME $_PATH"
# _1RIGHT="[%*] "

# bureau_precmd () {
#   _1SPACES=`get_space $_1LEFT $_1RIGHT`
#   print
#   print -rP "$_1LEFT$_1SPACES$_1RIGHT"
# }

# setopt prompt_subst
# PROMPT='> $_LIBERTY '
# RPROMPT='$(nvm_prompt_info) $(bureau_git_prompt)'

# autoload -U add-zsh-hook
# add-zsh-hook precmd bureau_precmd
