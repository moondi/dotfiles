# good references for when re-organizing:
# - https://unix.stackexchange.com/questions/71253/what-should-shouldnt-go-in-zshenv-zshrc-zlogin-zprofile-zlogout
# - https://medium.com/@rajsek/zsh-bash-startup-files-loading-order-bashrc-zshrc-etc-e30045652f2e
#
# +----------------+-----------+-----------+------+
# |                |Interactive|Interactive|Script|
# |                |login      |non-login  |      |
# +----------------+-----------+-----------+------+
# |/etc/zshenv     |    A      |    A      |  A   |
# +----------------+-----------+-----------+------+
# |~/.zshenv       |    B      |    B      |  B   |
# +----------------+-----------+-----------+------+
# |/etc/zprofile   |    C      |           |      |
# +----------------+-----------+-----------+------+
# |~/.zprofile     |    D      |           |      |
# +----------------+-----------+-----------+------+
# |/etc/zshrc      |    E      |    C      |      |
# +----------------+-----------+-----------+------+
# |~/.zshrc        |    F      |    D      |      |
# +----------------+-----------+-----------+------+
# |/etc/zlogin     |    G      |           |      |
# +----------------+-----------+-----------+------+
# |~/.zlogin       |    H      |           |      |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |~/.zlogout      |    I      |           |      |
# +----------------+-----------+-----------+------+
# |/etc/zlogout    |    J      |           |      |
# +----------------+-----------+-----------+------+

# basic OS detection; to be expanded as needed
if [[ $(uname -s) == 'Darwin' ]]; then
  export os_type='mac'
else
  if [[ -n "$WSL_DISTRO_NAME" ]]; then
    export os_type='windows'
  else
    export os_type='linux'
  fi
fi

# This is in here so it can be utilized by non-interactive shells, ie. Sublime and other editors, tooling, etc
# - gives a nice common baseline by which we can extend tooling (so long as we use `zsh` shebangs)
#   while still allowing devs to customize where they clone their repos to
export DEV=$HOME/dev

# macos fucks with your base in /etc/zprofile, which is immediately after this file in the processing order
# but this isn't needed for anything on my macs, so I'm OK with omitting it on them
if [[ "$os_type" != 'mac' ]]; then
  # anything installed into HOME should take precedence
  # - ATM, this is only for git-annex (b/c of permission issues with installing to /usr/local/bin on ubuntu in WSL2)
  export PATH="$HOME/bin:$HOME/.local/bin:$PATH"
fi

# source nix environment, if applicable
[ -e "$HOME/.nix-profile/etc/profile.d/nix.sh" ] && . "$HOME/.nix-profile/etc/profile.d/nix.sh"
