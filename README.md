- dotbot docs: https://github.com/anishathalye/dotbot
- bash cheatsheet: https://devhints.io/bash

### Install

```bash
#===============================================================
#                            SSH KEY
#                     ---------------------
# set up SSH key (I highly recommend creating a password, and storing in 1pass)
# - comment there is just a rough guideline, I like to use the year the machine was acquired
#   with additional suffixes as needed (ie. if installing different OSes)
ssh-keygen -t ed25519 -C "YYYY-machine-name"
cat ~/.ssh/id_ed25519.pub
# upload to gitlab SSH keys


#===============================================================
#                            DOTFILES
#                     ---------------------
git clone git@gitlab.com:moondi/dotfiles.git ~/dev/dotfiles && cd ~/dev/dotfiles

# ~~~~~~~~~~~~~~~~~~~~
# REQUIRED ADJUSTMENTS

# not using GPG/don't want to sign commits?
rm .gitconfig_gpg

# using GPG
# - import your GPG signing key onto the machine if you have one
# - if you want to create one (optional):
#   - this is good resource to create a long-lasting identity:
#     - https://alexcabal.com/creating-the-perfect-gpg-keypair
#   - another good reference:
#     - https://www.void.gr/kargig/blog/2013/12/02/creating-a-new-gpg-key-with-subkeys/
#   - **that is appropriate (IMO) if you don't use gpg for encrypting communication**
vim .gitconfig_gpg

# IDENTITIES
vim .gitconfig_identity # personal email should go here (or work, if you don't need multiple identities)
rm .gitconfig_work # pattern can be repeated if you're in orgs that require dedicated gitlab/github accounts
# ~~~~~~~~~~~~~~~~~~~~

./install

#=================================#
#     open a new terminal tab     #
#                                 #
#              done!              #
#=================================#
```

### Install without SSH (recommended for machines where you won't be making commits)

```bash
git clone https://gitlab.com/moondi/dotfiles.git ~/dev/dotfiles && cd ~/dev/dotfiles
rm .gitconfig_*
./install
```

### Update

```bash
git pull
./install
```

### Update dotbot

```bash
git submodule update --remote
# commit (ie. git commit -am "chore: update dotbot to vX.Y.Z")
./install
```

### Windows-exclusive settings

```bash
# IMPORTANT:
# Several things are working together to make this possible:
# - MSYS=winsymlinks:nativestrict (requires developer mode)
# - relative paths for the symlink
# - both locations must be on the same drive
#
# resources for additional context:
# - https://superuser.com/a/1400340
# - https://www.joshkel.com/2018/01/18/symlinks-in-windows/
# - https://stackoverflow.com/a/62031488

# turn on developer mode (atm, its here: Settings -> Update & Security > For Developers > Developer Mode)
git clone git@gitlab.com:moondi/dotfiles.git /mnt/c/dev/dotfiles && cd /mnt/c/dev/dotfiles
./install
```

### If there's conflicts with existing files

- check the differences
- encorporate any desirable changes in here
- then either:
  - delete the file on the local machine and re-install
  - or temporarily add `force: true` in install.conf.yaml and re-install (one-liners will need to be split, and the value stuck under `path: `)

### WIP - Committing guidelines

- committing to master is OK for isolated fixes/improvements (one commit only)
  - it might be worthwhile to adjust the pre-commit and pre-push hooks for this repo to accomodate that
- more than 1 commit related to an area? move to a branch
  - branches should never be merged manually; MR should always be created in gitlab
- [UNSURE] keep commits on master locally for a while before pushing?
  - only really viable while I'm working on a single machine
  - longer lead time means there's a higher likelyhood I'll do another adjustment or something in a similar area later
  - then those can be re-arranged and put into a branch
  - this might just be overkill though, it may just be better to commit and push master for one-off adjustments
