# zmodload zsh/zprof # Uncomment (+ paired last line to debug performance)

# good references for when re-organizing:
# - https://unix.stackexchange.com/questions/71253/what-should-shouldnt-go-in-zshenv-zshrc-zlogin-zprofile-zlogout
# - https://medium.com/@rajsek/zsh-bash-startup-files-loading-order-bashrc-zshrc-etc-e30045652f2e
#
# +----------------+-----------+-----------+------+
# |                |Interactive|Interactive|Script|
# |                |login      |non-login  |      |
# +----------------+-----------+-----------+------+
# |/etc/zshenv     |    A      |    A      |  A   |
# +----------------+-----------+-----------+------+
# |~/.zshenv       |    B      |    B      |  B   |
# +----------------+-----------+-----------+------+
# |/etc/zprofile   |    C      |           |      |
# +----------------+-----------+-----------+------+
# |~/.zprofile     |    D      |           |      |
# +----------------+-----------+-----------+------+
# |/etc/zshrc      |    E      |    C      |      |
# +----------------+-----------+-----------+------+
# |~/.zshrc        |    F      |    D      |      |
# +----------------+-----------+-----------+------+
# |/etc/zlogin     |    G      |           |      |
# +----------------+-----------+-----------+------+
# |~/.zlogin       |    H      |           |      |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |                |           |           |      |
# +----------------+-----------+-----------+------+
# |~/.zlogout      |    I      |           |      |
# +----------------+-----------+-----------+------+
# |/etc/zlogout    |    J      |           |      |
# +----------------+-----------+-----------+------+

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# macos fucks with your base PATH in /etc/zprofile, which is sourced immediately after ~/.zshenv
# so we're adding these to the path here instead (more details in ~/.zshenv)
if [[ "$os_type" == 'mac' ]]; then
  export PATH="$HOME/bin:$HOME/.local/bin:$PATH"
fi

# keychain
if [[ "$os_type" != 'mac' ]]; then
  # Add common SSH keys to keychain (there's probably a better way to do this...)
  [[ -f ~/.ssh/id_rsa ]] && eval $(keychain --eval --quiet id_rsa)
  [[ -f ~/.ssh/id_ed25519 ]] && eval $(keychain --eval --quiet id_ed25519)
fi

if [[ "$os_type" == 'windows' ]]; then
  # https://superuser.com/a/1340707
  export WINHOME=$(wslpath $(cmd.exe /C "echo %USERPROFILE%" 2>/dev/null | tr -d '\r'))

  # Allows proper symlinking - this requires developer mode to be enabled
  # details: https://superuser.com/a/1400340
  export MSYS=winsymlinks:nativestrict

  # so password prompt will be in-terminal, and thus actually work under WSL2 on win 10
  export GPG_TTY=$(tty)
fi

ZSH_THEME="moose"
HIST_STAMPS="yyyy-mm-dd"
COMPLETION_WAITING_DOTS="true"
setopt HIST_IGNORE_SPACE # prevent commands prefixed with a space from going into the command history; useful for dangerous commands!

# Recommended update process:
# - brew update
# - brew upgrade git zsh zsh-completions
plugins=(
  zsh-completions # first so it ends up towards the back of $FPATH
  git # aliases for git - ships with oh-my-zsh
  # official completions for zsh from git (also ships with oh-my-zsh)
  # - MUCH faster than the 'official completions for git from zsh'
  # additional context: https://stackoverflow.com/a/56483976
  gitfast
  docker # for completions
)

# enable option-stacking
# https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/docker#settings
zstyle ':completion:*:*:docker:*' option-stacking yes
zstyle ':completion:*:*:docker-*:*' option-stacking yes

mkdir -p "${HOME}/.oh-my-zsh/completions"
[[ -x "$(command -v brew)" ]] && FPATH="$(brew --prefix)/share/zsh/site-functions:${FPATH}"
[[ ! -e "${HOME}/.oh-my-zsh/completions/_kubectl" && -x "$(command -v kubectl)" ]] && kubectl completion zsh > "${HOME}/.oh-my-zsh/completions/_kubectl"
[[ ! -e "${HOME}/.oh-my-zsh/completions/_eksctl" && -x "$(command -v eksctl)" ]] && eksctl completion zsh > "${HOME}/.oh-my-zsh/completions/_eksctl"

source $ZSH/oh-my-zsh.sh

if [[ "$os_type" = 'mac' ]]; then
  # The next line updates PATH for the Google Cloud SDK.
  if [ -f "${HOME}/Applications/google-cloud-sdk/path.zsh.inc" ]; then . "${HOME}/Applications/google-cloud-sdk/path.zsh.inc"; fi

  # The next line enables shell command completion for gcloud.
  if [ -f "${HOME}/Applications/google-cloud-sdk/completion.zsh.inc" ]; then . "${HOME}/Applications/google-cloud-sdk/completion.zsh.inc"; fi

  if [[ -x "${HOME}/bin/aws_completer" ]]; then complete -C ${HOME}/bin/aws_completer aws; fi
fi

if [[ "$os_type" == 'mac' ]]; then
  export SUBL_PKG="$HOME/Library/Application Support/Sublime Text/Packages"
  export PATH="/Applications/Sublime Text.app/Contents/SharedSupport/bin:$PATH"
elif [[ "$os_type" == 'windows' ]]; then
  export SUBL_PKG="$WINHOME/AppData/Roaming/Sublime Text/Packages"
  export PATH="/mnt/c/Program Files/Sublime Text:$PATH"
  alias subl="subl.exe" # better than extending the PATH, as I don't want to type the extension for CLI usage
fi

source $HOME/.config/zsh/aliases.zsh

# export PATH="$PATH:$HOME/dev/git-scripts/moose:$HOME/dev/git-scripts"
# fpath=($HOME/dev/git-scripts/moose $HOME/dev/git-scripts $fpath)

unsetopt nomatch # workound the need to escape ^ (eg. in git reset head^) https://github.com/robbyrussell/oh-my-zsh/issues/449

eval "$(direnv hook zsh)"

# zprof # Uncomment (+ paired first line to debug performance)
